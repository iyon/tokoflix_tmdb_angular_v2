import { Injectable }               from '@angular/core';
import { Http, Response }           from '@angular/http';
import { Observable }               from 'rxjs/Observable';
import { Jsonp } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Movie } from './movie';

@Injectable()
export class MoviesService {
  private url = 'https://api.themoviedb.org/3/movie/';
  private searchUrl = 'https://api.themoviedb.org/3/search/movie';
  private apiKey = '3ef07924512ec1828abc2683f2a56374';
  private language;

  private apikey = '&api_key=3ef07924512ec1828abc2683f2a56374';
  private baseUrl = 'https://api.themoviedb.org/3/';
  private movie = 'movie/'
  private sortByPopularity = '&sort_by=popularity.desc';
  private jsonpCallback = '?callback=JSONP_CALLBACK';
  private sharedSearchResult: Array<Object> = [];
  private pageCount = '&page=1';
  private regionId = '&region=ID';
  private personId = 'person/';
  private cred = '&append_to_response=';

  constructor (private http: Http, private jsonp: Jsonp) {
  }

  public getNowPlayingMovies(): Observable<Movie[]> {
    let moviesUrl = `${this.url}now_playing?api_key=${this.apiKey}${this.regionId}`;
    return this.http.get(moviesUrl)      
    .map(this.extractData)
    .catch(this.handleError);
  }

  getMovies(): Observable<Movie[]> {
    let moviesUrl = `${this.url}popular?api_key=${this.apiKey}${this.regionId}`;

    return this.http.get(moviesUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }



  searchMovies(query: string) {
    let searchUrl = `${this.searchUrl}?api_key=${this.apiKey}&language=${this.language}&query=${query}`;

    return this.http.get(searchUrl)
      .map((res) => { return res.json() })
  }

  getDetails(id : number) {
    let detailsUrl = `${this.url}${id}?api_key=${this.apiKey}${this.regionId}`;

    return this.http.get(detailsUrl)
      .map((res) => { return res.json() })
  }

  // **** **** // 

  public getSharedSearchResult() {
    return this.sharedSearchResult;
  }

  public setSharedSearchResult(searchResult) {
    this.sharedSearchResult = searchResult;
  }

  public getPopularMovies() {
    return this.jsonp.get(this.baseUrl + this.movie + 'popular' + this.jsonpCallback + this.apikey + this.pageCount + this.regionId)
      .map(result => result.json())
  }

  public getTopRatedMovies() {
    return this.jsonp.get(this.baseUrl + this.movie + 'top_rated' + this.jsonpCallback + this.apikey + this.pageCount + this.regionId)
      .map(result => result.json())
  }

  public getUpComingMovies() {
    return this.jsonp.get(this.baseUrl + this.movie + 'upcoming' + this.jsonpCallback + this.apikey + this.pageCount + this.regionId)
      .map(result => result.json())
  }

 

  // public searchMovies(query) {
  //   return this.jsonp.get(this.baseUrl + 'search/movie' + this.jsonpCallback + '&query=' + query + this.sortByPopularity + this.apikey + this.pageCount + this.regionId)
  //     .map(result => result.json())
  // }

  public getMovieDetails(id) {
    return this.jsonp.get(this.baseUrl + this.movie + id + this.jsonpCallback + this.apikey + this.pageCount + this.regionId)
      .map(result => result.json())
  }

  public getSimilarMovies(id) {
    return this.jsonp.get(this.baseUrl + this.movie +  id + '/similar' +this.jsonpCallback + this.apikey + this.pageCount + this.regionId)
      .map(result => result.json())
  }

  public getMovieReviews(id) {
    return this.jsonp.get(this.baseUrl + this.movie + id + '/reviews' +this.jsonpCallback + this.apikey + this.pageCount + this.regionId)
      .map(result => result.json())
  }





  private extractData(res: Response) {
    let body = res.json();
    return body.results || { };
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
